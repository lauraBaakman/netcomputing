package Server;

import Client.DataPacket;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Accepts the incoming connections serves them off to a new thread.
 *
 * Accepts all the incoming connections and spawns a thread to handle the
 * connections. All connections will be handled as a DataPacket, from Client.
 *
 * @author guus
 */
public class ServerReceiver extends Thread {

    ServerSocket serverSocket;
    private ConcurrentLinkedQueue<DataPacket> queue = new ConcurrentLinkedQueue<DataPacket>();

    /**
     * Constructor
     *
     * @param s the server socket to be used
     */
    public ServerReceiver(ServerSocket s) {
        super();
        serverSocket = s;
    }

    /**
     * Accepts incoming connections and serves them to a new thread. Runs
     * while(true).
     */
    @Override
    public void run() {
        while (true) {
            try {
                new ServerReceiverThread(serverSocket.accept(), queue).run();
            } catch (IOException ex) {
                ex.printStackTrace();
                Logger.getLogger(ServerReceiver.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Returns the data that was gathered from the incoming connections and
     * clears the queue.
     *
     * @return The data that was gathered from the incoming connecting.
     */
    public ArrayList<DataPacket> getQueue() {
        ArrayList<DataPacket> copy;

        synchronized (queue) {
            copy = new ArrayList<DataPacket>(queue);
        }
        queue.clear();
        return copy;
    }
}
