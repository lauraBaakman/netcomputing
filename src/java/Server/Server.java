package Server;

import RMI.rmiHeatChart;
import Client.DataPacket;
import GPS.GPSPoint;
import HeatMap.HeatChart;
import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.rmi.Naming;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Central server class that spawns a thread to receive connections and
 * distributes the work.
 *
 * Although this way of working has sort of man in the middle smell, it does
 * allow for better division of labor
 *
 * @author Guus Klinkenberg & Laura Baakman
 */
public class Server {

    private ServerReceiver connectionThread;
    static private int port = 7983;
    private int rasterSize = 5;
    private GPSPoint sw = new GPSPoint(0, 0);
    private GPSPoint ne = new GPSPoint(50, 50);
    private HeatChart map;
    private rmiHeatChart remoteObject;

    /**
     * Constructor the never releases. Starts the server, creates a thread to
     * handle all the incoming connections and then loops (indefinitely): sleep
     * for approx 20 seconds, gets data from the thread that handles incoming
     * connections and generates a heatmap from that.
     *
     * @param s socket
     */
    public Server(ServerSocket s) {
        connectionThread = new ServerReceiver(s);


        //Init RMI Stuff
        try {
            remoteObject = (rmiHeatChart) Naming.lookup("//localhost/RmiServer");
            remoteObject.init(new Double(Math.max(sw.x, ne.x)).intValue(), new Double(Math.max(sw.y, ne.y)).intValue());
        } catch (Exception ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }


    }

    /**
     * Starts the never ending loop of this server.
     */
    public void startServer() {
        connectionThread.start();
        System.out.println("Ready...");

        while (true) {
            try {
                Thread.sleep(2 * 1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            }

            ArrayList<DataPacket> copy = connectionThread.getQueue();

            if (!copy.isEmpty()) {
                try {
                    this.map = remoteObject.generateHeatChart(copy);
                    this.map.saveToFile(new File("web/images/java-heat-chart.png"));
                } catch (Exception ex) {
                    Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                }
                System.out.println("Made heatmap, if i'm right");
            } else {
                System.out.println("Made no heatmap since there were no data");
            }
        }
    }

    /**
     * Generate a server and start it.
     *
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = null;

        try {
            Server server = new Server(new ServerSocket(port));
            server.startServer();
        } catch (IOException e) {
            System.err.println("Could not listen on port: " + port);
            System.exit(-1);
        }
    }
}
