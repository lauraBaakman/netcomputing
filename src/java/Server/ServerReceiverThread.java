package Server;

import Client.DataPacket;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author guus
 */
public class ServerReceiverThread implements Runnable {

    private Queue<DataPacket> queue;
    private Socket socket;

    /**
     *
     * @param s
     * @param queue
     */
    public ServerReceiverThread(Socket s, Queue<DataPacket> queue) {
        this.socket = s;
        this.queue = queue;
    }

    /**
     *
     */
    @Override
    public void run() {
        processClient(socket);
    }

    private void processClient(Socket socket) {
        PrintWriter out;
        ObjectInputStream in;

        try {
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new ObjectInputStream(socket.getInputStream());
        } catch (IOException ex) {
            ex.printStackTrace();
            Logger.getLogger(ServerReceiver.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        Object input;
        DataPacket casted;

        try {
            input = in.readObject();
            casted = (DataPacket) input;
            synchronized (queue) {
                queue.add(casted);
            }
        } catch (Exception ex) {
            Logger.getLogger(ServerReceiverThread.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            try {
                out.close();
                in.close();
                socket.close();
            } catch (IOException ex) {
                Logger.getLogger(ServerReceiverThread.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            }
        }

    }
}
