package Client;

import GPS.GPSPoint;
import java.awt.Point;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author guus
 */
public class ClientSendToServer implements Runnable {

    final private String serverAddress = "localhost";
    final private int port = 7983;
    final private double maxOffsetCoords = 0.000045; // about 5 meters. Emulator value.
    final private int maxOffsetMeters = 3;
    final private int rasterSize = 1; // distance in meters
    private String id;
    private GPSPoint sw;
    private GPSPoint ne;
    private GPSPoint clientLocation;

    /**
     * Constructor that will work within area between (0,0) and (50,50) and
     * picks a random location within that area. Invokes initialize() with these
     * variables
     */
    public ClientSendToServer() {
        GPSPoint sw = new GPSPoint(0, 0);
        GPSPoint ne = new GPSPoint(50, 50);
        initialize(GPSPoint.getRandomLocationInArea(sw, ne), sw, ne);
    }

    /**
     * Constructor which gives the most control. Invokes initialize.
     *
     * @param clientLocation location of the client at the moment. May not be
     * null
     * @param southWest Southwest corner of the area the client needs to stay in
     * to send data to the server.
     * @param northEast Northeast corner of the area the client needs to stay in
     * to send data to the server.
     */
    public ClientSendToServer(GPSPoint clientLocation, GPSPoint southWest, GPSPoint northEast) {
        initialize(clientLocation, southWest, northEast);
    }

    /**
     * Sets the attributes of the class.
     *
     * @param clientLocation Location where the client is.
     * @param southWest Southwest corner of the area the client needs to stay in
     * to send data to the server.
     * @param northEast Northeast corner of the area the client needs to stay in
     * to send data to the server.
     */
    private void initialize(GPSPoint clientLocation, GPSPoint southWest, GPSPoint NorthEast) {
        this.clientLocation = clientLocation;
        sw = southWest;
        ne = NorthEast;
        id = Integer.toString(new Random().nextInt(Integer.MAX_VALUE));
    }

    /**
     * Main method for ClientSendToServer starts a client
     *
     * @param args
     */
    public static void main(String[] args) {
        new ClientSendToServer().run();
    }

    private void sendDataToServer() {
        ObjectOutputStream out = null;
        try {
            Socket socket = new Socket(serverAddress, port);
            out = new ObjectOutputStream(socket.getOutputStream());
            System.out.println("Went to point " + clientLocation + "to server");
            Point rasterIndex = clientLocation.locationOnRaster(sw, rasterSize);
            out.writeObject(new DataPacket(id, rasterIndex));
        } catch (Exception ex) {
            Logger.getLogger(ClientSendToServer.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }

        try {
        } catch (Exception ex) {
            Logger.getLogger(ClientSendToServer.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
        System.out.println("Sent new location to server");
    }

    /**
     * Makes the thread: - Check within the defined area, if not, sleep for an
     * iteration - Connect to the server - Convert it's coordinates to an index
     * on the raster for the heatmap - Send these coordinates to the server
     */
    @Override
    public void run() {
        while (true) {
            clientLocation = clientLocation.getRandomLocationNear(maxOffsetMeters);

            if (!clientLocation.onLocation(sw, ne)) {
                String msg = "Client was not in area between " + sw + " and " + ne + " with location " + clientLocation;
            } else {
                sendDataToServer();
            }

            try {
                Thread.sleep(2 * 1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(ClientSendToServer.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            }
        }
    }
}
