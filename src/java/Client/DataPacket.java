package Client;

import java.awt.Point;
import java.io.Serializable;

/**
 * Datapacket that is used to send from client to the server.
 *
 * @author guus
 */
public class DataPacket implements Serializable {

    private String id;
    private Point coordinates;

    /**
     * Wrapper constructor to parse the input received from client to
     *
     * @param id Client ID
     * @param coordinates The current location of the client
     */
    public DataPacket(String id, Point coordinates) {
        this.id = id;
        this.coordinates = coordinates;
    }

    /**
     * Get the value of coordinates
     *
     * @return the value of coordinates
     */
    public Point getCoordinates() {
        return coordinates;
    }

    /**
     * Set the value of coordinates
     *
     * @param coordinates new value of coordinates
     */
    public void setCoordinates(Point coordinates) {
        this.coordinates = coordinates;
    }

    /**
     * Get the value of id
     *
     * @return the value of id
     */
    public String getId() {
        return id;
    }

    /**
     * Set the value of id
     *
     * @param id new value of id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Returns a string representation of the datapacket
     *
     * @return
     */
    @Override
    public String toString() {
        return "DataPacket{" + "id=" + id + ", coordinates=" + coordinates + '}';
    }
}
