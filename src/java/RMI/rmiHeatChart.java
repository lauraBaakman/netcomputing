/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package RMI;

import Client.DataPacket;
import HeatMap.HeatChart;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 *
 * @author laura
 */
public interface rmiHeatChart extends Remote {

    /**
     * Initialization function
     *
     * @param maxX
     * @param maxY
     * @throws RemoteException
     */
    void init(int maxX, int maxY) throws RemoteException;

    /**
     * Generates a heat chart
     *
     * @param inputData
     * @return
     * @throws RemoteException
     */
    HeatChart generateHeatChart(ArrayList<DataPacket> inputData) throws RemoteException;
}
