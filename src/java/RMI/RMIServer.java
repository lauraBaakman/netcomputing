package RMI;

import Client.DataPacket;
import HeatMap.HeatChart;
import java.awt.Color;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.registry.*;
import java.util.ArrayList;

/**
 * RMI Server that implements the interface rmiHeatChart
 *
 * @author laura
 */
public class RMIServer extends UnicastRemoteObject implements rmiHeatChart {

    private int maxX;
    private int maxY;

    /**
     * Constructor for this server
     *
     * @throws RemoteException
     */
    public RMIServer() throws RemoteException {
        super(0);    // required to avoid the 'rmic' step, see below
    }

    /**
     * Starts and RMIserver with its registry on port 1099
     *
     * @param args
     * @throws Exception
     */
    public static void main(String args[]) throws Exception {
        System.out.println("RMI server started");

        try {
            //special exception handler for registry creation
            LocateRegistry.createRegistry(1099);
            System.out.println("java RMI registry created.");
        } catch (RemoteException e) {
            //do nothing, error means registry already exists
            System.out.println("java RMI registry already exists.");
        }

        //Instantiate RmiServer
        RMIServer obj = new RMIServer();

        // Bind this object instance to the name "RmiServer"
        Naming.rebind("//localhost/RmiServer", obj);
        System.out.println("PeerServer bound in registry");
    }

    /**
     *
     * @param maxX The maximum x coordinate in the heatmaps to be generated by
     * this server
     * @param maxY The maximum y coordinate in the heatmaps to be generated by
     * this server
     * @throws RemoteException
     */
    @Override
    public void init(int maxX, int maxY) throws RemoteException {
        this.maxX = maxX;
        this.maxY = maxY;
    }

    /**
     *
     * @param inputData The data based on which a heatmap should be generated
     * @return A HeatChart with the lowValue color green and the high value
     * color red
     * @throws RemoteException
     */
    @Override
    public HeatChart generateHeatChart(ArrayList<DataPacket> inputData) throws RemoteException {
        double data[][] = new double[maxX][maxY];
        for (DataPacket packet : inputData) {
            data[packet.getCoordinates().x][packet.getCoordinates().y]++;
        }
        HeatChart map = new HeatChart(data);
        map.setLowValueColour(Color.green);
        map.setHighValueColour(Color.red);
        map.setShowXAxisValues(false);
        map.setShowYAxisValues(false);
        return map;
    }
}
