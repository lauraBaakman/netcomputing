# README
This readme is for the project "overcrowding" by Guus Klinkenberg & Laura Baakman. The project is meant as a project/Proof of Concept for the principle of detecting overcrowding (too many people in a area that is too small) using various webtechnologies, such as servlets and a RMI computing engine, and relies on current mobile technology (namely smartphones with a GPS sender).

## How to set up
To use the code, download/clone the repository and place the folders "src" and "web" in the project folder for a netbeans project which was created using the name "overcrowding" using the /overcrowding domain. This is important as a lot of files rely on this linkage.

## How to run
Run the files in this order

	0. Start rabbitmq server on the host
    1. RMIServer
    2. Server
    3. As many ClientSendToServer as you want
    4. The Project

# RESTHallo
To run RESTHallo, ensure that the jars in web/lib are included and that tomcat is configured.
