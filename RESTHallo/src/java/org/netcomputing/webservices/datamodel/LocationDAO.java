package org.netcomputing.webservices.datamodel;

import java.util.HashMap;
import java.util.Map;

/**
 * Singleton designpattern The possible implementation of Java depends on the
 * version of Java you are using. As of Java 6 you can define singletons with a
 * single-element enum type. This way is currently the best way to implement a
 * singleton in Java 1.6 or later according to the book ""Effective Java from
 * Joshua Bloch.
 */

public enum LocationDAO {
	instance;

	private Map<String, Location> contentProvider = new HashMap<String, Location>();

	private LocationDAO() {
		Location p = new Location("01", 53.456, 6.678);
		contentProvider.put("1", p);
	}

	public Map<String, Location> getModel() {
		return contentProvider;
	}

}