package org.netcomputing.webservices.client;

import java.net.URI;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.netcomputing.webservices.datamodel.Location;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.representation.Form;

public class LocationClient {

		public static void main(String[] args) {
			ClientConfig config = new DefaultClientConfig();
			Client client = Client.create(config);
			WebResource service = client.resource(getBaseURI());
			
			// Create a Location
			Location l = new Location ("Kim",53.14,7.0);
			ClientResponse response = service.path("rest").path("locations")
					.path(l.getId()).accept(MediaType.APPLICATION_XML)
					.put(ClientResponse.class, l);
//			// Return code should be 201 == created resource
//			System.out.println(response.getStatus());
//			// Get the Locations
//			System.out.println(service.path("rest").path("locations")
//					.accept(MediaType.TEXT_XML).get(String.class));
//			// Get JSON for application
//			System.out.println(service.path("rest").path("locations")
//					.accept(MediaType.APPLICATION_JSON).get(String.class));
//			// Get XML for application
//			System.out.println(service.path("rest").path("locations")
//					.accept(MediaType.APPLICATION_XML).get(String.class));
//
//			// Get the location with id 1
//			System.out.println(service.path("rest").path("locations/1")
//					.accept(MediaType.APPLICATION_XML).get(String.class));
//			// get location with id 1
//			service.path("rest").path("locations/Kim").delete();
//			// Get the all locations , id 1 should be deleted
//			System.out.println(service.path("rest").path("locations")
//					.accept(MediaType.APPLICATION_XML).get(String.class));
//
//			// Create a Location
//			Form form = new Form();
//			form.add("id", "hack");
//			form.add("latitude", new Double("50.1234"));
//			form.add("latitude", new Double("6.1234"));
//			response = service.path("rest").path("locations")
//					.type(MediaType.APPLICATION_FORM_URLENCODED)
//					.post(ClientResponse.class, form);
//			System.out.println("Form response " + response.getEntity(String.class));
//			// Get the all locations, id 4 should be created
//			System.out.println(service.path("rest").path("locations")
//					.accept(MediaType.APPLICATION_JSON).get(String.class));

		}

		private static URI getBaseURI() {
			return UriBuilder.fromUri("http://localhost:8080/RESTHallo/").build();
			
		}
	} 