package org.netcomputing.webservices.server;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;

import org.netcomputing.webservices.datamodel.Location;
import org.netcomputing.webservices.datamodel.LocationDAO;


public class LocationResource {
	@Context
	UriInfo uriInfo;
	@Context
	Request request;
	String id;
	public LocationResource(UriInfo uriInfo, Request request, String id) {
		this.uriInfo = uriInfo;
		this.request = request;
		this.id = id;
	}
	
	//Application integration 		
	@GET
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Location getLocation() {
		Location p = LocationDAO.instance.getModel().get(id);
		if(p==null)
			throw new RuntimeException("Get: location with " + id +  " not found");
		return p;
	}
	
	// For the browser
	@GET
	@Produces(MediaType.TEXT_XML)
	public Location getLocationHTML() {
		Location p = LocationDAO.instance.getModel().get(id);
		if(p==null)
			throw new RuntimeException("Get: Location with " + id +  " not found");
		return p;
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_XML)
	public Response putLocation(JAXBElement<Location> todo) {
		Location c = todo.getValue();
		return putAndGetResponse(c);
	}
	
	@DELETE
	public void deleteLocation() {
		Location c = LocationDAO.instance.getModel().remove(id);
		if(c==null)
			throw new RuntimeException("Delete: Location with " + id +  " not found");
	}
	
	private Response putAndGetResponse(Location l) {
		Response res;
		if(LocationDAO.instance.getModel().containsKey(l.getId())) {
			res = Response.noContent().build();
		} else {
			res = Response.created(uriInfo.getAbsolutePath()).build();
		}
		LocationDAO.instance.getModel().put(l.getId(), l);
		return res;
                
	}
	
	

} 