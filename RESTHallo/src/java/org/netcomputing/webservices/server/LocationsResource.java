package org.netcomputing.webservices.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import org.netcomputing.webservices.datamodel.Location;
import org.netcomputing.webservices.datamodel.LocationDAO;


//Will map the resource to the URL positions
@Path("/locations")
public class LocationsResource {
	// Allows to insert contextual objects into the class,
	// e.g. ServletContext, Request, Response, UriInfo 
	
	@Context UriInfo uriInfo;
	@Context
	Request request;

	// Return the list of Locations to the user in the browser 
	
	@GET
	@Produces(MediaType.TEXT_XML)
	public List<Location> getLocationsBrowser() {
		List<Location> Locations = new ArrayList<Location>();
		Locations.addAll(LocationDAO.instance.getModel().values());
		return Locations;
	}

	// Return the list of Locations for applications 
	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public List<Location> getLocations() {
		List<Location> Locations = new ArrayList<Location>();
		Locations.addAll(LocationDAO.instance.getModel().values());
		return Locations;
	}

	// retuns the number of Locations
	// Use
	// http://localhost:8080/NetComputing_04_LocationWS/rest/locations/count
	// to get the total number of records @GET @Path("count")
	@Produces(MediaType.TEXT_PLAIN)
	public String getCount() {
		int count = LocationDAO.instance.getModel().size();
		return String.valueOf(count);
	}
	
	@POST
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public void newLocation(@FormParam("id") String id,
			@FormParam("latitude") Double latitude,
			@FormParam("longitude") Double longitude,
			@Context HttpServletResponse servletResponse) throws IOException {
		Location Location = new Location(id, latitude, longitude);
		LocationDAO.instance.getModel().put(id, Location);
		servletResponse.sendRedirect("../rest/locations");
	}

	// Defines that the next path parameter after Locations is
	// treated as a parameter and passed to the LocationResources
	// Allows to type
	// http://localhost:8080/NetComputing_04_LocationWS/rest/locations/1
	// 1 will be treaded as parameter Location and passed to LocationResource
	@Path("{location}")
	public LocationResource getLocation(@PathParam("location") String id) {
		return new LocationResource(uriInfo, request, id);
	}
}
