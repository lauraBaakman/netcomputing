package rest;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.representation.Form;
import java.net.URI;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

public class RESTClient {

    public static void main(String[] args) {
        ClientConfig config = new DefaultClientConfig();
        Client client;
        client = Client.create(config);
        WebResource service = client.resource(getBaseURI());

        // Create a Location
//        ClientResponse response = service.path("webresources").path("hello")
//                .path(l.getId()).accept(MediaType.APPLICATION_XML)
//                .put(ClientResponse.class, l);
        ClientResponse response = service.path("webresources").path("hello").accept(MediaType.APPLICATION_XML).put(ClientResponse.class, "test");
        // Return code should be 201 == created resource
        System.out.println(response.getStatus());

    }

    private static URI getBaseURI() {
        return UriBuilder.fromUri("http://localhost:8080/RESTHallo/").build();

    }
}

