/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rest;

import java.util.ArrayList;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author guus
 */
@Path("hello")
public class helloResource {

    @Context
    private UriInfo context;

    private ArrayList<String> list = new ArrayList<String>();
    
    /**
     * Creates a new instance of helloResource
     */
    public helloResource() {
        list.add("3");
        list.add("2");
        list.add("1");
        list.add("BOEM!");
        list.add("");
    }

    /**
     * Retrieves representation of an instance of rest.helloResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("text/html")
    public String getHtml() {
        //TODO return proper representation object
        return "<h1>Hello</h1> THis is an HTML page... oooooooooh<br>" + list;
    }

    /**
     * PUT method for updating or creating an instance of helloResource
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes(MediaType.TEXT_XML)
    public void putHtml(String content) {
        list.add(content);
    }
}
